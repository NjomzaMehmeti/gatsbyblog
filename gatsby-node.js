const path = require('path');

module.exports.onCreateNode = ({ node, actions}) =>{

    const { createNodeField } = actions

    if(node.internal.type == "MarkdownRemark"){
        //extract the file name from absolute path
        const slug = path.basename(node.fileAbsolutePath, '.md')

        //create a slug for each post
        createNodeField({
            node,
            name: 'slug',
            value: slug
        })
    }
}

//Generating a new page for each post

module.exports.createPages = async ({ graphql, actions}) =>{

    const { createPage } = actions

    //Dynamically create pages
    //1. Get path to template
    const blogTemplate = path.resolve('./src/blog-templates/blog.js');
    //2. Get markdown Data
   const res = await graphql(`
       query {
           allMarkdownRemark {
               edges {
                   node{
                       fields {
                           slug
                       }
                   }
               }
           }
       }
    
    `)
    //then iterate over these pages
    res.data.allMarkdownRemark.edges.forEach((edge) => {
        createPage({
            component: blogTemplate,
            // path hits the route -> localhost:8000/blog/firstPage
            path: `/blog/${edge.node.fields.slug}`,
            //stuff to pass down to the template ,whether slug , whether optional text
            context: {
                    slug: edge.node.fields.slug
            }
        })
    });
    //3. Create new pages
}

// module.exports.onCreatePage = ({ page }) =>{
//     if(page.path.startsWith('/404')){
//         page.layout = '404';
//     }
// }