import React from 'react'
import Layout from '../components/layout'
import { graphql , useStaticQuery, Link} from 'gatsby'
import blogStyle from './blog.module.scss'

const BlogPage = ()=>{

const data = useStaticQuery(graphql`

 {
    allMarkdownRemark {
      edges {
        node {
          frontmatter {
            author
            title
            date(formatString: "MMMM Do, YYYY")
          }
          fields {
            slug
          }
          excerpt(pruneLength: 30)
        }
      }
    }
  }
`)



    return(
        
            <Layout>
              <div>
            <h1>This is the Blog Page</h1>
            <h4>Below you can find the GraphQL data from markdown files</h4>
            
            <ol className={blogStyle.posts}>{data.allMarkdownRemark.edges.map((item,index)=>{
                        return(
                    //React identifies items with unique keys so it's better for DOM manipulation
                           <li key={index} className={blogStyle.post}>
                              <p>Posted by {item.node.frontmatter.author} on {item.node.frontmatter.date}</p>

                               <h2>{item.node.frontmatter.title}</h2>
                               <p>{item.node.excerpt}</p>
                              
                               <Link to={item.node.fields.slug}>Read More</Link> 
                           </li>
                        )
            })}</ol>
            </div>
            </Layout> 
           
        
    )
}

//allmarkdownremark - takes all the posts whereas markdown.. a single one

export default BlogPage