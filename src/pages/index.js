import React from "react"
import Layout from '../components/layout'
const IndexPage = () => {
return(
  <Layout>
  <h1>Hello, This is an experiment with Gatsby Js</h1>
     <h4>Made by Njomza Mehmeti</h4>
      {/* LinkComponent- better optimization, no refresh, better UX*/}
      <p>You can check out the Blog Page by clicking above!</p>
  </Layout>
)

}

export default IndexPage
