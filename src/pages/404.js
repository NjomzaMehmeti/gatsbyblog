import React from 'react'
import NF from '../styles/404.module.scss';
import { Link } from 'gatsby';

const NotFound = () => {
    return(
      <div className={NF.container}>
      <div className={NF.innercontainer}>
        <div className={NF.notfound}>
          <h1>404</h1>
          <h2>Page not found</h2>
        </div>
       <Link to='/'>Homepage</Link>
      </div>
    </div>
    )
}

export default NotFound