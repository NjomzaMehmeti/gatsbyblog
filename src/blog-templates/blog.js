import React from 'react'
import Layout from '../components/layout'
import { graphql } from 'gatsby'

export const query = graphql`
    query(
        $slug: String!
      ) {
        markdownRemark(
          fields: {
            slug: {
              eq: $slug
            }
          }
        ) {
          frontmatter {
            title
            author
            date(formatString: "DD-MM-YYYY")
          }
          html
        }
      }
    
    `

const Blog = (props) => {

    
    return(
        <Layout>
          <h5>Posted by {props.data.markdownRemark.frontmatter.author} on {props.data.markdownRemark.frontmatter.date}</h5>
            <h1>{props.data.markdownRemark.frontmatter.title}</h1>
            {/* render HTML text */}
            <div dangerouslySetInnerHTML={ {__html: props.data.markdownRemark.html}}></div>
        </Layout>
    )

}

export default Blog