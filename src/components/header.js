import React from 'react'
import { Link, graphql, useStaticQuery } from 'gatsby'
import headerStyles from '../styles/header.module.scss'
const Header = ()=>{
    const data = useStaticQuery(graphql`
          query {
              site {
                  siteMetadata {
                      title
                  }
              }
          }
    `)
    return(
        <header className={headerStyles.header}>

            <h1>
                <Link className={headerStyles.logoSize} to="/">
                   {data.site.siteMetadata.title}
                </Link>
            </h1>

            <nav>
                <ul className={headerStyles.navList}>
                    <li><Link className={headerStyles.navItem} activeClassName={headerStyles.activeNavItem} to="/">Index Page</Link></li>
                    <li><Link className={headerStyles.navItem} activeClassName={headerStyles.activeNavItem} to="/blog">Blog Page</Link></li>
                </ul>
            </nav>
        </header>
    )
}

export default Header
