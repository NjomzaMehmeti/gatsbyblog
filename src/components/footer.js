import React from 'react'
import { graphql, useStaticQuery } from 'gatsby'
import FooterStyle from '../styles/footer.module.scss';

const Footer = () => {

    const data = useStaticQuery(graphql`
    query {
        site {
            siteMetadata {
                author
            }
        }
    }
`)


    return(
        <div className={FooterStyle.container}>
            <footer>This blog is made by {data.site.siteMetadata.author}, Copyright &copy; {new Date().getFullYear()}</footer>
        </div>
    )
}

export default Footer