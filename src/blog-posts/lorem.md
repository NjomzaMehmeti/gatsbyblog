---
date: "2020-09-19"
title: "Lorem Ipsum"
author: "Doxa"
---

<img src="./dhermi.jpg" alt="Dhermia" width="200px" height="150px"/>

<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem  industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem..</p>

<h4>Loving what you do is the only way to survive all these obstacles.
As cliche as it sounds, sometimes this is simply just truth. If you love the path you are taking, love the job you are doing, love the direction you are going… you don’t need acknowledgements from the outside world.This kind of fulfillment cannot be borrowed or replaced, or even worse, faked.</h4>

The most difficult things about learning to code by yourself — and how to tackle them<br>
<a href='/blog'>Back</a>